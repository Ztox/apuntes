using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource),typeof(Collider2D))]
public class Keys : MonoBehaviour
{

    private static int keys = 0;

    private AudioSource audioKey;

    private void Awake()
    {
        keys++;
        audioKey = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            audioKey.Play();
            GetComponent<Collider2D>().enabled = false;
            keys--;
            Ready();
            if (transform.childCount > 0)
                Destroy(transform.GetChild(0).gameObject);
            Destroy(gameObject, 2);
        }
    }

    private void Ready()
    {
        if (keys == 0)
        {
            EventManager.instance.Keys();
        }
    }
}
