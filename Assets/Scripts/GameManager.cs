using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private Transform point;

    private Transform player;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        player = PlayerController.instance;
    }

    public void SetPoint(Transform _position)
    {
        point = _position;
    }

    public void Death()
    {
        // float distance = Mathf.Infinity;
        // int pos=0;

        // for (int i = 0; i < points.Length; i++)
        // {
        //     if(points[i]!=null)
        //     {   
        //         float _distTemp;
        //         _distTemp = Vector3.Distance(points[i].position,player.position);
        //         if(_distTemp<distance)
        //         {
        //             pos=i;
        //         }
        //     }
        // }
        player.GetComponent<PlayerController>().Respawn(point.position);
        player.eulerAngles = Vector3.zero;
        player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
    }
    public void Limit()
    {
        player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        player.GetComponent<Rigidbody2D>().AddTorque(-500);
    }
}
