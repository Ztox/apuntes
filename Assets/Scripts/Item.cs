using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public enum Form
    {
        jump,
        speed,
        attack,
        stamina,
    }

    [SerializeField]
    private Form item;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerController>() != null)
        {
            switch (item)
            {
                case Form.jump:
                other.GetComponent<PlayerController>().JumpItem();
                break;
                case Form.speed:
                other.GetComponent<PlayerController>().RunItem();
                break;
                case Form.attack:
                other.GetComponent<PlayerController>().AttackItem();
                break;
                case Form.stamina:
                other.GetComponent<PlayerController>().Resistitem();
                break;
                
            }
            Destroy(gameObject);
        }
    }
}


