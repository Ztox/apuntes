using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{

    public static EventManager instance;

    [SerializeField]
    private float delaySoundKey = 1.5f;

    [SerializeField]
    private AudioSource dropKey;

    [SerializeField]
    private AudioSource OpenDoor;

    [SerializeField]
    private GameObject block;

    [SerializeField]
    private GameObject lamp;

    [SerializeField]
    private Bulds[] bulds = new Bulds[0];

    [SerializeField]
    private Bulds[] buldEnd = new Bulds[0];

    private void Awake()
    {
        instance = this;
    }

    IEnumerator Start()
    {
        yield return new WaitForSeconds(delaySoundKey);
        dropKey.Play();
    }

    public void Keys()
    {
        StartCoroutine(delayOpenDoor());
        if (lamp != null)
            lamp.SetActive(true);
        Destroy(block);
    }

    IEnumerator delayOpenDoor()
    {
        yield return new WaitForSeconds(1);
        OpenDoor.Play();
    }

    public void Boo()
    {
        StartCoroutine(BooDelay(bulds, 1));
    }

    IEnumerator BooDelay(Bulds[] _bulds, float _time)
    {
        foreach (var i in _bulds)
        {
            i.BrokenBuld();
            yield return new WaitForSeconds(_time);
        }

    }

    public GameObject sparkle;

    public void SecondKey()
    {
        if (sparkle != null)
            sparkle.SetActive(true);
    }

    public void BooEnd()
    {
        StartCoroutine(BooDelay(buldEnd, 1f));
        Finish.GetComponent<Animator>().enabled = true;
        Finish.GetComponent<AudioSource>().enabled = true;
        StartCoroutine(DelayFinish());
    }

    public Bulds Finish;
    public AudioSource backGround;

    IEnumerator DelayFinish()
    {
        yield return new WaitForSeconds(8);
        Finish.BrokenBuld();
        for (int i = 100; i > 0; i--)
        {
            if (i != 0)
                backGround.volume = i / 100;
            else
                backGround.volume = 0;
            yield return new WaitForSeconds(5 / 100);
        }
        yield return new WaitForSeconds(5);
        Application.Quit();
    }
}
