using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZone : MonoBehaviour
{
    public enum Tip
    {
        limit,
        death,
    }

    public Tip carTM;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (carTM == Tip.death)
        {
            if (other.CompareTag("Player"))
            {
                GameManager.instance.Death();
            }
        }else
        {
            if (other.CompareTag("Player"))
            {
                GameManager.instance.Limit();
            }
        }
    }
}
