using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    public enum Zone
    {
        first,
        second,
        third,
        four,
    }

    public Zone order;

    public float time = 1;

    private float timeD;

    private void Awake()
    {
        timeD = Time.fixedDeltaTime;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        switch (order)
        {
            case Zone.first:
                if (other.CompareTag("Player"))
                {
                    EventManager.instance.Boo();
                    Destroy(gameObject);
                }
                break;
            case Zone.second:

                StartCoroutine(Slow());
                GetComponent<Collider2D>().enabled = false;
                break;
            case Zone.third:
                EventManager.instance.SecondKey();
                Destroy(gameObject);
                break;
            case Zone.four:
                PlayerController.instance.GetComponent<PlayerController>().End();
                EventManager.instance.BooEnd();
                Destroy(gameObject);
                break;

        }


    }

    IEnumerator Slow()
    {
        Time.timeScale = 0.2f;
        Time.fixedDeltaTime = timeD * Time.timeScale;
        yield return new WaitForSecondsRealtime(time);
        Time.timeScale = 1;
        Time.fixedDeltaTime = timeD * Time.timeScale;
    }
}
