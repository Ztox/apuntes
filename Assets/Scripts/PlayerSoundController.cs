using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayerSoundController : MonoBehaviour
{
    [Header("Steps")]

    [SerializeField]
    private AudioClip[] stepsWalk = new AudioClip[0];

    [SerializeField]
    private AudioClip[] stepsRun = new AudioClip[0];

    [Header("Pitchs")]

    [SerializeField]
    private float pitchWalking = 0.7F;

    [SerializeField]
    private float pitchRunning = 1.3F;

    private int lastStep = 0;

    private AudioSource AudioPlayer;

    private void Awake()
    {
        AudioPlayer = GetComponent<AudioSource>();
    }

    public void Step(string _type)
    {
        if (_type == "Walk")
        {
            if (stepsWalk[lastStep] == null)
            {
                return;
            }
            
            AudioPlayer.pitch = pitchWalking;

            AudioPlayer.clip = stepsWalk[lastStep];
        }

        if (_type == "Run")
        {
            if (stepsRun[lastStep] == null)
            {
                return;
            }

            AudioPlayer.pitch = pitchRunning;

            AudioPlayer.clip = stepsRun[lastStep];
        }

        AudioPlayer.Play();
        lastStep++;

        if (lastStep == stepsWalk.Length)
        {
            lastStep = 0;
        }


    }

}
