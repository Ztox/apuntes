using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    public static Transform instance;

    [SerializeField]
    private float jumpForce = 100;

    [SerializeField]
    private float speed = 2.5f;

    [SerializeField]
    private float runSpeed = 3.5f;

    [SerializeField]
    private bool canAttack = false;

    [SerializeField]
    private bool canJump = false;

    [SerializeField]
    private bool canResist = false;

    [SerializeField]
    private bool canRun = false;

    [SerializeField]
    private float ext;

    private bool isPlayerMoving = false;

    [SerializeField]
    private LayerMask groundLayer;

    private Rigidbody2D body;

    private Animator animator;

    private RaycastHit2D groundCheck;

    private CapsuleCollider2D corp;

    private float timeD;

    [SerializeField]
    private PlayerSoundController playerSounds;

    private void Awake()
    {
        instance = transform;
        body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        corp = GetComponent<CapsuleCollider2D>();
        timeD = Time.fixedDeltaTime;
    }

    private void Update()
    {
        float direction = Input.GetAxis("Horizontal");


        groundCheck = Physics2D.Raycast(transform.position, Vector3.down, ext, groundLayer);

        PlayerMovementVerification();

        Move(direction);

        animator.SetBool("InGround", groundCheck);

        // if (canAttack)
        // {
        //     if (Input.GetButtonDown("Attack"))
        //     {
        //         Attack();
        //     }
        // }

        if (canJump && groundCheck)
        {
            if (Input.GetButtonDown("Jump"))
            {

                Jump();
            }
        }
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(1))
        {
            Time.timeScale = 0.2f;
        }
        if (Input.GetMouseButtonUp(1))
        {
            Time.timeScale = 1;
        }
        Time.fixedDeltaTime = timeD * Time.timeScale;
#endif

    }

    private void PlayerMovementVerification()
    {
        if (Input.GetAxisRaw("Horizontal") != 0)
            isPlayerMoving = true;
        else
            isPlayerMoving = false;
    }

    public float tempSpeed = 0.5f;

    public float tempSpeedChange = 0.05f;

    public float tempMinFlip = 0.2f;

    private void Move(float _direction)
    {

        if (Mathf.Abs(body.velocity.x) > tempMinFlip)
        {
            Flip(Mathf.Sign(body.velocity.x));
            // Flip(Mathf.Sign(_direction));
        }


        float _speed;

        if (Input.GetButton("Run") && canRun)
        {
            _speed = runSpeed;
            // animator.SetFloat("xVelocity", Mathf.Abs(_direction * 2));
        }
        else
        {
            _speed = speed;
            // animator.SetFloat("xVelocity", Mathf.Abs(_direction));
        }


        animator.SetFloat("yVelocity", body.velocity.y);

        if (!groundCheck)
        {
            body.velocity = new Vector2(Mathf.Lerp(body.velocity.x, _direction * _speed, 0.02f), body.velocity.y);
        }
        else
        {
            body.velocity = new Vector2(Mathf.Lerp(body.velocity.x, _direction * _speed, tempSpeed), body.velocity.y);
            // body.velocity = new Vector2(_direction * _speed, body.velocity.y);
        }

        float _xVelocity = Mathf.Lerp(animator.GetFloat("xVelocity"), Mathf.Abs(body.velocity.x / speed), tempSpeedChange);

        if (body.velocity.x != 0)
            animator.SetFloat("xVelocity", _xVelocity);
        else
            animator.SetFloat("xVelocity", 0);
        // Debug.Log(body.velocity.x);
        if (_xVelocity < 0.01f && Mathf.Abs(Input.GetAxisRaw("Horizontal"))<1)
            animator.SetFloat("xVelocity", 0);
    }

    private void Flip(float _direction)
    {
        // Debug.Log(Mathf.Round(_direction));
        transform.localScale = new Vector3(_direction, 1, 1);
    }

    private void Jump()
    {
        animator.SetTrigger("Jump");
        body.velocity = new Vector2(body.velocity.x, 0);
        body.AddForce(Vector2.up * jumpForce);
    }
    private void Attack()
    {
        animator.SetTrigger("Attack");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Destroyable"))
        {
            Destroy(other.gameObject);
        }
    }

    public void Respawn(Vector3 _position)
    {
        transform.position = _position;
    }

    public void JumpItem()
    {
        canJump = true;
        Jump();
    }

    public void RunItem()
    {
        canRun = true;
    }

    public void AttackItem()
    {
        canAttack = true;
    }

    public void Resistitem()
    {
        canResist = true;
    }

    public void End()
    {
        Move(0);
        body.velocity = Vector3.zero;
        animator.SetBool("InGround", true);
        animator.SetFloat("xVelocity", 0);
        animator.SetFloat("yVelocity", 0);
        this.enabled = false;

    }

    private void StepsWalking()
    {
        if (playerSounds == null && groundCheck == false)
        {
            return;
        }

        playerSounds.Step("Walk");

    }

    private void StepsRunnig()
    {
        if (playerSounds == null && groundCheck == false)
        {
            return;
        }

        playerSounds.Step("Run");

    }
}
