using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Bulds : MonoBehaviour
{
    [SerializeField]
    private AudioClip broken;

    [SerializeField]
    private AudioSource sound;
    
    public void BrokenBuld()
    {
        if (broken != null)
        {
            GetComponent<UnityEngine.Rendering.Universal.Light2D>().enabled = false;
            GetComponent<AudioSource>().enabled = false;
            sound.clip = broken;
            sound.Play();
        }
    }


}
